﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using ControlWork6New.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ControlWork6New.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        public OrderController(ApplicationDbContext context)
        {
            this.context = context;
        }
        // GET: api/Order
        [HttpGet]
        public string GetOrders()
        {
            var order = context.Order.ToList();
            return JsonConvert.SerializeObject(order, Formatting.Indented);

        }

        // POST: api/Order
        [HttpPost]
        public string Post([FromForm] Orders order)
        {
            context.Order.Add(order);
            context.SaveChanges();
            return "Success";
        }

        
        // PUT: api/Order/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
