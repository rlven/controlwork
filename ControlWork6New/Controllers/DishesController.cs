﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork6New.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ControlWork6New.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class DishesController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        public DishesController(ApplicationDbContext context)
        {
            this.context = context;
        }
        // GET: api/Dishes
        [HttpGet]
        public string GetDishes()
        {
            var dish = context.Dish.ToList();
            return JsonConvert.SerializeObject(dish, Formatting.Indented);

        }

        // POST: api/Dishes
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Dishes/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
