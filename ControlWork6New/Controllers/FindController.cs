﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ControlWork6New.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ControlWork6New.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FindController : ControllerBase
    {
        private readonly ApplicationDbContext context;
        public FindController(ApplicationDbContext context)
        {
            this.context = context;
        }
        // GET: api/Find
        [HttpGet]
        public string GetDish(string name)
        {
            foreach (var cont in context.Dish)
            {
                if (cont.Name == name)
                {
                    var dish = cont;
                    return JsonConvert.SerializeObject(dish, Formatting.Indented);
                }
            }
            return "cant find object";
        }

        // POST: api/Find
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Find/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
