﻿using System.ComponentModel.DataAnnotations;

namespace ControlWork6New.Models
{
    public class Client
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
    }
}