﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ControlWork6New.Models
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Cafe> Cafe { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Dish> Dish { get; set; }
        public DbSet<Orders> Order { get; set; }
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Cafe>()
                .HasData(JsonConvert.DeserializeObject<Cafe[]>(File.ReadAllText("Seeds/cafe.json")));
            modelBuilder.Entity<Client>()
                .HasData(JsonConvert.DeserializeObject<Client[]>(File.ReadAllText("Seeds/client.json")));
            modelBuilder.Entity<Dish>()
                .HasData(JsonConvert.DeserializeObject<Dish[]>(File.ReadAllText("Seeds/dish.json")));
            modelBuilder.Entity<Orders>()
                .HasData(JsonConvert.DeserializeObject<Orders[]>(File.ReadAllText("Seeds/order.json")));

            modelBuilder.Entity<Cafe>()
                .HasMany(c => c.Dishes)
                .WithOne(c => c.Cafe)
                .HasPrincipalKey(c => c.Id)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Dish>()
                .HasOne(d => d.Cafe)
                .WithMany(d => d.Dishes)
                .HasForeignKey(d => d.CafeId);

        }
    }
}
