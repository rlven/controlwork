﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ControlWork6New.Models
{
    public class Dish
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        [ForeignKey("Cafe")]
        public int CafeId { get; set; }
        public Cafe Cafe { get; set; }
        public string Description { get; set; }
        public override string ToString()
        {
            return $"Name: {Name} - Price: {Price} - Cafe: {CafeId} - Description: {Description} ";
        }
    }
}