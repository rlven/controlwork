﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ControlWork6New.Models
{
    public class Orders 
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Client")]
        [FromForm(Name = "clientId")]
        public int ClientId { get; set; }
        public Client Client { get; set; }
        [ForeignKey("Dish")]
        [FromForm(Name = "dishId")]
        public int DishId { get; set; }
        public Dish Dish { get; set; }
        [FromForm(Name = "date")]
        public DateTime OrderDate { get; set; }
    }
}