﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ControlWork6New.Models
{
    public class Cafe
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<Dish> Dishes { get; set; }

    }
}