﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ControlWork6New.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cafe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cafe", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Contact = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Dish",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    CafeId = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Dish", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Dish_Cafe_CafeId",
                        column: x => x.CafeId,
                        principalTable: "Cafe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Order",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClientId = table.Column<int>(nullable: false),
                    DishId = table.Column<int>(nullable: false),
                    OrderDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Order", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Order_Client_ClientId",
                        column: x => x.ClientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Order_Dish_DishId",
                        column: x => x.DishId,
                        principalTable: "Dish",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Cafe",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Italian", "ChefPizza" },
                    { 2, "Mexican", "MuchachoBurger" },
                    { 3, "FastFood", "Mc-Donalds" }
                });

            migrationBuilder.InsertData(
                table: "Client",
                columns: new[] { "Id", "Contact", "Name" },
                values: new object[,]
                {
                    { 1, "0552-44213-31", "Lina" },
                    { 2, "0552-44213-31", "Alex" },
                    { 3, "0552-44213-31", "Whitney" }
                });

            migrationBuilder.InsertData(
                table: "Dish",
                columns: new[] { "Id", "CafeId", "Description", "Name", "Price" },
                values: new object[] { 1, 1, "HotFish", "FriedFish", 30m });

            migrationBuilder.InsertData(
                table: "Dish",
                columns: new[] { "Id", "CafeId", "Description", "Name", "Price" },
                values: new object[] { 2, 2, "HotSoup", "Soup", 60m });

            migrationBuilder.InsertData(
                table: "Dish",
                columns: new[] { "Id", "CafeId", "Description", "Name", "Price" },
                values: new object[] { 3, 3, "HotChicken", "Chicken", 330m });

            migrationBuilder.InsertData(
                table: "Order",
                columns: new[] { "Id", "ClientId", "DishId", "OrderDate" },
                values: new object[] { 1, 1, 1, new DateTime(2018, 2, 3, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Order",
                columns: new[] { "Id", "ClientId", "DishId", "OrderDate" },
                values: new object[] { 2, 2, 2, new DateTime(2018, 1, 4, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "Order",
                columns: new[] { "Id", "ClientId", "DishId", "OrderDate" },
                values: new object[] { 3, 3, 3, new DateTime(2018, 5, 8, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.CreateIndex(
                name: "IX_Dish_CafeId",
                table: "Dish",
                column: "CafeId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_ClientId",
                table: "Order",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Order_DishId",
                table: "Order",
                column: "DishId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Order");

            migrationBuilder.DropTable(
                name: "Client");

            migrationBuilder.DropTable(
                name: "Dish");

            migrationBuilder.DropTable(
                name: "Cafe");
        }
    }
}
